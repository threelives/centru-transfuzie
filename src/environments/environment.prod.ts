export const environment = {
  production: true,
  API_URL: 'https://threelives-api.solomons.work/',
  SESSION_PREFIX: 'transfusion'
};
