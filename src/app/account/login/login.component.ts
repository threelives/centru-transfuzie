import { AuthService } from './../../auth/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AccountService } from '../services/account.service';
import { ToastrService } from 'ngx-toastr';
import { ApiCodes } from 'src/app/codes/api-codes.enum';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  private loginForm: FormGroup;

  private submitted = false;

  public loading = false;

  public get f() {
    return this.loginForm.controls;
  }

  constructor(
    private accountService: AccountService,
    private authService: AuthService,
    private toastr: ToastrService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.loginForm = new FormBuilder().group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  submit() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return false;
    }

    this.accountService.login({
      email: this.f.email.value,
      password: this.f.password.value,
    }).subscribe((response: any) => {
      if (response.token) {
        this.toastr.success('Login succeeded');

        this.authService.setToken(response.token.accessToken);

        this.router.navigate(['/']);
      } else {
        this.toastr.error(response.error);
      }
    }, (response) => {
      if (response.status === ApiCodes.NOT_AUTHORIZED) {
        this.toastr.error('Nume utilizator sau parola incorecta', 'Esuat');
      } else {
        this.toastr.error(response.error.error, 'Eroare');
      }
    });
  }
}
