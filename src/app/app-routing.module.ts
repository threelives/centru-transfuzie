import { AuthGuard } from './auth/guard/guard';
import { LoginComponent } from './account/login/login.component';
import { RouterModule, Route } from '@angular/router';
import { NgModule } from '@angular/core';
import { RegisterComponent } from './account/register/register.component';
import { HomeViewComponent } from './pages/home/home-view/home-view.component';

const routes: Route[] = [
    {
        path: '',
        component: HomeViewComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'login',
        component: LoginComponent,
    },
    {
        path: 'register',
        component: RegisterComponent,
    },
    {
        path: '**',
        redirectTo: ''
    }
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }