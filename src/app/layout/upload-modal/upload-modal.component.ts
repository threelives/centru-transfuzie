import { AuthService } from './../../auth/services/auth.service';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { environment } from 'src/environments/environment';
import { DropzoneComponent, DropzoneConfigInterface } from 'ngx-dropzone-wrapper';

@Component({
  selector: 'app-upload-modal',
  templateUrl: './upload-modal.component.html',
  styleUrls: ['./upload-modal.component.scss']
})
export class UploadModalComponent implements OnInit {
  public config: DropzoneConfigInterface;

  @ViewChild(DropzoneComponent, { static: false }) componentRef?: DropzoneComponent;
  
  constructor(
    public activeModal: NgbActiveModal,
    public authService: AuthService
  ) { }

  public ngOnInit() {
    const token = this.authService.getToken();

    this.config = {
      url: `${environment.API_URL}api/transfusion-center/import`,
      withCredentials: false,
      clickable: true,
      maxFiles: null,
      autoReset: null,
      errorReset: null,
      cancelReset: null,
      headers: {
        'Cache-Control': null,
        'X-Reauest-With': null
      }
    };

    if (token) {
      this.config.headers["Authorization"] = "Bearer " + token;
    }
  }

  dismiss(data) {
    this.activeModal.close(data);
  }
  
  public onUploadInit(args: any): void {
    console.log('onUploadInit:', args);
    // this.loading = true;
  }

  public onUploadError(args: any): void {
    // this.loading = false;
    console.log('onUploadError:', args);
  }

  public onUploadSuccess(args: any): void {
    // this.loading = false;
    
    /* if (response.success) {
      
    } else {
      this.toastr.error(response.data.error, 'Eroare');
    } */
  }
}
