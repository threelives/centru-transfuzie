import { AuthService } from './../../auth/services/auth.service';
import { UploadModalComponent } from './../upload-modal/upload-modal.component';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(
    private modal: NgbModal,
    private authService: AuthService
  ) { }

  ngOnInit() {
  }

  async uploadForm() {
    const modalRef = this.modal.open(UploadModalComponent, {
      backdrop: 'static'
    });
    /* modalRef.result.then((result: any) => {
      
    }); */
  }

  logout() {
    this.authService.logout();
  }
}
