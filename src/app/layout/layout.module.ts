import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { IsBusyComponent } from './is-busy/is-busy.component';
import { UploadModalComponent } from './upload-modal/upload-modal.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';

const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
  maxFilesize: 50,
  acceptedFiles: '*'
};

@NgModule({
  declarations: [HeaderComponent, FooterComponent, SidebarComponent, IsBusyComponent, UploadModalComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    DropzoneModule
  ],
  exports: [
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    IsBusyComponent,
    UploadModalComponent
  ],
  entryComponents: [
    UploadModalComponent
  ]
})
export class LayoutModule {
  constructor() {
    library.add(faTimes);
  }
}
