import { environment } from './../../../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class HomeService {
    constructor(
        private http: HttpClient
    ) {

    }

    getRecords() {
        return this.http.get(environment.API_URL + 'api/transfusion-center/donations').toPromise();
    }
}