import { MatTableModule, MatTableDataSource, MatPaginatorModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeViewComponent } from './home-view/home-view.component';
import { DataSource } from '@angular/cdk/table';

@NgModule({
  declarations: [HomeViewComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule
  ],
  exports: [HomeViewComponent]
})
export class HomeModule { }
