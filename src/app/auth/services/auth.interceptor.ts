import { Injectable } from '@angular/core';

import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
    constructor(private auth: AuthService, private toaster: ToastrService, private router: Router) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (req.method !== 'OPTIONS') {
            req = req.clone({
                withCredentials: true
            });

            return next.handle(req)
                .pipe(catchError((error, caught) => {
                    this.handleAuthError(error);
                    return of(error);
                }) as any);
        }
        return next.handle(req);
    }

    private handleAuthError(err: HttpErrorResponse): Observable<any> {
        // handle your auth error or rethrow
        if (err.status === 401) {
            // navigate /delete cookies or whatever
            // console.log('handled error ' + err.status);
            this.router.navigate(['/logout']);
            this.toaster.error('Session Expired', 'Not authorized');
            // if you've caught / handled the error, you don't want to rethrow it
            // unless you also want downstream consumers to have to handle it as well.
            return of(err.message);
        } else {
            // console.log(err);
            if (err.status === 0 && err.statusText === 'Unknown Error') {
                this.toaster.error('Service unavailable, please try again later.', 'Error');
            } else {
                this.toaster.error(err.message, 'Error');
            }
        }
        throw err;
    }
}