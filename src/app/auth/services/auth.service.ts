import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public isLoggedInSubject: BehaviorSubject<boolean>;
  public isLoggedIn: Observable<boolean>;

  private token = null;

  constructor(
    private router: Router
  ) {
    this.setObservables();
  }

  hasSession(route: Router, state: string) {
    const token = localStorage.getItem(environment.SESSION_PREFIX + '_token');

    if (token) {
      this.isLoggedInSubject.next(true);

      return of(true);
    }

    return route.navigate(['/login']);
  }

  private setObservables() {
    // Is logged in observable
    this.isLoggedInSubject = new BehaviorSubject<boolean>(false);
    this.isLoggedIn = this.isLoggedInSubject.asObservable();

    // Role observable
    // this.roleSubject = new BehaviorSubject<Roles>(Roles.GUEST);
    // this.role = this.roleSubject.asObservable();

    // Name observable
    // this.fullNameSubject = new BehaviorSubject<string>('');
    // this.fullName = this.fullNameSubject.asObservable();
  }

  setToken(token: string) {
    localStorage.setItem(environment.SESSION_PREFIX + '_token', token);

    this.isLoggedInSubject.next(true);
  }

  getToken() {
    const token = localStorage.getItem(environment.SESSION_PREFIX + '_token');

    if (token) {
      return token;
    }

    return null;
  }

  logout() {
    localStorage.removeItem(environment.SESSION_PREFIX + '_token');
    this.isLoggedInSubject.next(false);
    this.router.navigate(['/login']);
  }
}
